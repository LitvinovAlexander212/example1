#include <stdio.h>
#include <time.h>

int it_F(int i, int Fi, int Fii){
  int buf;
  while (i!=0){
    buf=Fii;
    Fii=Fi;
    Fi=Fi+buf;
    i--;
  }
  return Fi;
}

int re_F(int i){
   return (( i < 2 )? (i==0) ? 0 : 1 : re_F(i-2)+re_F(i-1));
}

int main() {
  clock_t start, end;
  int i, Fi=0, Fii=1;
  printf("Enter i-th member of Fibonachi sequence\n" );
  scanf("%d\n", &i);
  start = clock();
  printf("\n1) %d\n", it_F(i, Fi, Fii));
  end = clock();
  printf("Time = %ld\n",(end - start) / (CLOCKS_PER_SEC));
  start = clock();
  printf("\n2) %d\n", re_F(i));
  end = clock();
  printf("Time = %ld\n",(end - start) / (CLOCKS_PER_SEC));
  return 0;
}

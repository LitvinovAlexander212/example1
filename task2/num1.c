#include <stdio.h>

double sqrt_f(double x0, double x, double eps){
  double xi;
  xi=(x0+x/x0)/2;
  if (((xi-x0)>-eps)&&((xi-x0)<eps)){
    return(xi);
  }
  sqrt_f(xi,x,eps);
}

int main() {
  double x;
  double eps;
  printf("\nEnter eps\t");
  scanf("%lf", &eps);
  printf("\nEnter numbers, then press ENTER and CTRL+D\t");
  while (scanf("%lf\n", &x)!=EOF){
    printf("\nsqrt(%.1lf) = %.10lf\n", x, sqrt_f(1,x,eps));
  }
  return 0;
}
/*

*/

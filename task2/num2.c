#include <stdio.h>
int main() {
  double x, ai, Px;
  double PPx, PPx1, PPx2, PPx3; /* Для подсчета производной
  без использования функций, массивов и указателей*/
  double IPx, IPx1; /*Для подсчета интеграла*/
  int n;
  printf("\nEnter polynomial degree and sequence : N X A(n) A(n-1) ... A0 \n" );
  scanf("%d%lf%lf", &n, &x, &Px);
  PPx1=Px;
  PPx2=Px;
  PPx=0;
  PPx3=0;
  IPx1=Px;
  IPx=0;
  while (scanf("%lf", &ai)!=EOF){
      if (n==0) {
        IPx+=IPx1*x;
      }
      else {
        IPx1=(ai)/(n+1);
      }
      /*
      Px*=x;
      Px+=ai;

      PPx1+=PPx3;
      PPx1+=PPx;
      PPx3=PPx1*x-PPx1;
      PPx*=x;
      PPx=(PPx2*x+ai);
      PPx2=PPx;

  }
  printf("\nP(%lf)=%lf\n", x, Px);
  printf("\nP'(%lf)=%lf\n", x, PPx1);
  printf("\nINTEGRAL OF P(x)dx by 0 to %lf=%lf\n", x, IPx);
  return 0;
}

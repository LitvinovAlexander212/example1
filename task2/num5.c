#include <stdio.h>
#include <malloc.h>

typedef struct node {
  int val;
  struct node * next;
} node_t;

void print_list(node_t * head){
  node_t * current = head;
  while (current != NULL) {
    printf("%d\n", current -> val);
    current = current -> next;
  }
}

int pop(node_t ** head){
  int retval =-1;
  node_t * next_node = NULL;
  if (*head == NULL){
    return -1;
  }
  next_node = (*head) -> next;
  retval = (*head) -> val;
  free(*head);
  *head = next_node;
  return retval;
}

int remove_by_value(node_t ** head, int value){
  int retval =-1;
  node_t * last = *head;
  if (*head == NULL){
    return -1;
  }
  node_t * current = *head;
  while (last->next != NULL){
    last = last -> next;
  }
  if ((current -> val) == (last -> val)){
    pop(&current);
  }
  node_t * precurrent = *head;
  if (current != NULL)&&(current -> next != NULL) {
    current = *head -> next
  }
}


void push_end_list(node_t * head, int valu){
  node_t * current = head;
  while (current -> next !=NULL) {
    current = current -> next;
  }
  current -> next = malloc(sizeof(node_t));
  current -> next -> val = valu;
  current -> next -> next = NULL;
}

int main() {
  node_t * l = NULL;
  l = malloc(sizeof(node_t));
  if (l==NULL){
    return 1;
  }
  l->val = 3;
  l->next = NULL;
  push_end_list(l, 10);
  push_end_list(l,100);
  print_list(l);
  pop(&l);

  print_list(l);
  return 0;
}

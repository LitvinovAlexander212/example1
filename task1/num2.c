#include <stdio.h>

int main(){
  printf("size (int) =%ld\n", sizeof(int));
  printf("size (long int) =%ld\n", sizeof(long int));
  printf("size (char) =%ld\n", sizeof(char));
  printf("size (signed char) =%ld\n", sizeof(signed char));
  printf("size (unsigned char) =%ld\n", sizeof(unsigned char));
}
